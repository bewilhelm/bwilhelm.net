var dateFormat = require('dateformat')

module.exports = function(date, format) {
    return dateFormat(date, format)
}
