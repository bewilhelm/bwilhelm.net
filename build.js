const buildType = process.argv.length > 2 ? process.argv[2] : 'dev'
switch (buildType) {
  case 'dev':
    devBuild = true;
    serve = false;
    break;
  case 'serve':
    devBuild = true;
    serve = true;
    break;
  case 'production':
    devBuild = false;
    serve = false;
    break;
  default:
    throw "Unknown build type: " + buildType
}

metalsmith = require('metalsmith')

// Markdown to HTML
var markdown = require('metalsmith-markdown')
var layouts = require('metalsmith-layouts')
var partials = require('metalsmith-discover-partials')
var helpers = require('metalsmith-discover-helpers')
var htmlmin = require('metalsmith-html-minifier')
var transform = require('metalsmith-dom-transform')
var codeHighlight = require(__dirname + '/lib/code-highlight')

// Site structure
var collections = require('metalsmith-collections')
var permalinks = require('metalsmith-permalinks')
var redirect = require('metalsmith-redirect')
var sitemap = require('metalsmith-mapsite')

// Posts
var excerpts = require('metalsmith-excerpts')
var rssfeed = require('metalsmith-feed')

// CSS
var sass = require('metalsmith-sass')
var filedata = require('metalsmith-filedata')

// Assets
// var assets = require('metalsmith-assets')
var favicons = require('metalsmith-favicons')

// Drafts and publish
var publish = require('metalsmith-publish')
var browsersync = require('metalsmith-browser-sync')

// Metadata and basics
var dir = {
  source: './src',
  destination: './public'
}
var siteMeta = {
  name: "Benjamin Wilhelm",
  description: "Benjamin Wilhelm's website and blog. Articles about machine learning, computer vision, web-development, git and other computer science related stuff I discover during my day as a full time nerd.",
  generator: "Metalsmith",
  url: serve ? 'http://127.0.0.1' : 'https://bwilhelm.net',
  source: "https://gitlab.com/bewilhelm/bwilhelm.net",
}
var ms = metalsmith(__dirname)
  .metadata({
    site_meta: siteMeta,
    date_format: "yyyy-mm-dd",
    github_username: "HedgehogCode"
  })
  .source(dir.source)
  .destination(dir.destination)
  .clean(true)

// Favicons
ms.use(favicons({
  src: 'favicon.svg',
  dest: 'favicons/',
  appName: siteMeta.name,
  appDescription: siteMeta.description,
  icons: {
    android: true,
    appleIcon: true,
    favicons: true,
    windows: true
  }
}))

// Collections of pages and posts
ms.use(publish({
  private: devBuild
}))
  .use(collections({
    pages: {
      pattern: 'pages/*',
      sortBy: 'priority',
      reverse: true,
      refer: false
    },
    posts: {
      pattern: '{posts/*,drafts/*}',
      sortBy: 'date',
      refer: true
    }
  }))

// CSS
ms.use(sass({
  outputDir: ''
}))
  .use(filedata({
    pattern: 'style.css',
    key: 'style'
  }))

// Markdown to HTML
ms.use(markdown())
ms.use(excerpts())

// Permalinks
ms.use(permalinks({
  pattern: ':title',
  relative: false,
  // duplicatesFail: true,

  // each linkset defines a match, and any other desired option
  linksets: [
    {
      match: { collection: 'posts' },
      pattern: 'posts/:date/:title',
    },
  ]
}))

// Layout
ms.use(partials())
  .use(helpers())
  .use(layouts({
    default: 'page.hbs',
    pattern: "**/*.html"
  }))

// Highlight source code
ms.use(transform({
  transforms: [
    codeHighlight({
      languages: []
    })
  ]
}))

// Minify html
if (!devBuild) {
  ms.use(htmlmin({
    minifierOptions: {
      removeAttributeQuotes: false
    }
  }))
}

// Sitemap and RSS feed
ms.use(sitemap({
  hostname: siteMeta.url,
  omitIndex: true
}))
ms.use(rssfeed({
  collection: 'posts',
  destination: 'feed.xml',
  site_url: siteMeta.url,
  title: siteMeta.name,
  description: siteMeta.description
}))

// Redirect old links
ms.use(redirect({
  frontmatter: true
}))

// Development
if (serve) {
  ms.use(browsersync({
    server: dir.destination,
    files: [dir.source + '**/*']
  }))
}

// Build
ms.build(function (err, files) {
  if (err) { throw err; }
});
