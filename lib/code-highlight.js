/*
* Copied and adapted from https://github.com/fortes/metalsmith-code-highlight
* Copyright: Filipe Fortes, License: ISC
*/
const highlight = require('highlight.js');

module.exports = function (options) {
  highlight.configure(options);

  return function highlightContent(root, data, metalsmith, done) {
    Array.from(root.querySelectorAll('code')).forEach(node => {
      highlight.highlightBlock(node);
    });

    done();
  };
};
