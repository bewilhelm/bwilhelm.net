# My website

[![pipeline status](https://gitlab.com/bewilhelm/bwilhelm.net/badges/master/pipeline.svg)](https://gitlab.com/bewilhelm/bwilhelm.net/commits/master)

## Development

Make sure you have `node-lts` and `yarn` installed.

Install the requirements:
```
$ yarn install
```

Serve the site locally:
```
$ yarn serve
```

Build:
```
$ yarn build
```

Test:
```
$ yarn test
```

## Update

Keep the site up to date:
```
$ yarn upgrade
```

## License

The content of the website is licensed under the [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/). (Markdown files and directories `src/pages` and `src/posts`)

The underlying source code used to display and format the content is licensed under the [MIT License](http://opensource.org/licenses/mit-license.php). (HTML, SCSS and files)

